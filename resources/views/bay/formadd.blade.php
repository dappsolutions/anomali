<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="Bay.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Bay</h3>
                        </div>
                        <hr>
                        <form>                           
                            <div class="form-group has-success">                                
                                <label for="gardu_induk" class="control-label mb-1">GI</label>
                                <select name="gardu_induk" id="gardu_induk" class="form-control required" error="GI">
                                    @foreach ($list_gardu as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($gardu_induk))
                                            @if ($gardu_induk == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif                                            
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_gardu'] }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="upt"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="nama_bay" class="control-label mb-1">Nama Bay</label>
                                <input id="nama_bay" name="nama_bay" type="text" class="form-control required"
                                    error="nama_bay" value="{{ isset($nama_bay) ? $nama_bay : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="nama_bay"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div>
                                <button id="save-button" onclick="Bay.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
