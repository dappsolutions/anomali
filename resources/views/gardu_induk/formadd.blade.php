<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="GarduInduk.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Gardu Induk</h3>
                        </div>
                        <hr>
                        <form>                           
                            <div class="form-group has-success">                                
                                <label for="upt" class="control-label mb-1">UPT</label>
                                <select name="upt" id="upt" class="form-control required" error="UPT">
                                    @foreach ($list_upt as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($upt))
                                            @if ($upt == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif                                            
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_upt'] }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="upt"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="nama_gardu" class="control-label mb-1">Nama GI</label>
                                <input id="nama_gardu" name="nama_gardu" type="text" class="form-control required"
                                    error="nama_gardu" value="{{ isset($nama_gardu) ? $nama_gardu : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="nama_gardu"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div>
                                <button id="save-button" onclick="GarduInduk.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
