<div class="row">
    <div class="col-md-12">
        <div class="card border">
            <div class="card-header">
                <strong>Map</strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" id="map-container">
                            <div id="map" style="" class=""></div>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-3">
                        <select name="" id="upt" multiple="multiple" onchange="Dashboard.getFilterUpt(this)">
                            <option value="">Pilih UPT</option>
                            @foreach ($list_upt as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_upt }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
