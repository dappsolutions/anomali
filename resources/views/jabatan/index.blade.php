@if (isset($akses->jabatan))
    <input type="hidden" name="" id="akses-update" value="{{ $akses->jabatan->update }}">
    <input type="hidden" name="" id="akses-delete" value="{{ $akses->jabatan->delete }}">
    <div class="row">
        <div class="col-md-12">
            <div class="card border">
                <div class="card-header">
                    <strong class="card-title">Daftar Data</strong>
                    @if ($akses->jabatan->create == 1)
                        <br>
                        <small><span class="btn btn-outline-primary btn-sm float-right mt-1"
                                onclick="Jabatan.add(this)">Tambah
                                Baru</span></small>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table-data-gardu-induk" class="table-thin table-striped table-bordered">
                            <thead>
                                <tr class="bg-yellow-apps-bold">
                                    <th class="th-padd">No</th>
                                    <th class="th-padd">Nama Jabatan</th>
                                    <th class="text-center th-padd">-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3" class="text-center td-padd">Tidak ada data ditemukan</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    @include('informasi.index')
@endif
