<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="Jabatan.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Jabatan</h3>
                        </div>
                        <hr>
                        <form>     
                            <div class="form-group has-success">
                                <label for="nama_jabatan" class="control-label mb-1">Nama Jabatan</label>
                                <input id="nama_jabatan" name="nama_jabatan" type="text" class="form-control required"
                                    error="nama_jabatan" value="{{ isset($nama_jabatan) ? $nama_jabatan : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="nama_jabatan"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div>
                                <button id="save-button" onclick="Jabatan.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
