<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                @if (Cache::store('file')->get('token') != '')
                    <li class="active">
                        <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="menu-title">Master</li><!-- /.menu-title -->
                    @if (isset($akses->gardu_induk))
                        <li class="">
                            <a href="{{ url($akses->gardu_induk->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->gardu_induk->nama_menu }}</a>
                        </li>
                    @endif
                    @if (isset($akses->bay))
                        <li class="">
                            <a href="{{ url($akses->bay->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->bay->nama_menu }}</a>
                        </li>
                    @endif
                    @if (isset($akses->phasa))
                        <li class="">
                            <a href="{{ url($akses->phasa->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->phasa->nama_menu }}</a>
                        </li>
                    @endif
                    @if (isset($akses->alat))
                        <li class="">
                            <a href="{{ url($akses->alat->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->alat->nama_menu }}</a>
                        </li>
                    @endif

                    <li class="menu-title">Data</li><!-- /.menu-title -->
                    @if (isset($akses->departemen))
                        <li class="">
                            <a href="{{ url($akses->departemen->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->departemen->nama_menu }}</a>
                        </li>
                    @endif
                    @if (isset($akses->jabatan))
                        <li class="">
                            <a href="{{ url($akses->jabatan->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->jabatan->nama_menu }}</a>
                        </li>
                    @endif
                    @if (isset($akses->pegawai))
                        <li class="">
                            <a href="{{ url($akses->pegawai->url_menu) }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>{{ $akses->pegawai->nama_menu }}</a>
                        </li>
                    @endif
                    <li class="menu-title">Anomali</li>
                    <li class="">
                        <a href="{{ url('anomali/critical_bay') }}"><i
                                class="menu-icon fa fa-file-text-o"></i>Criticality Bay </a>
                    </li>
                    <li class="">
                        <a href="{{ url('anomali/anomali') }}"><i
                                class="menu-icon fa fa-file-text-o"></i>Anomali</a>
                    </li>
                    @if (Cache::store('file')->get('akses') == 'superadmin')
                        <li class="menu-title">Setting</li>
                        <li class="">
                            <a href="{{ url('setting/akses_menu') }}"><i
                                    class="menu-icon fa fa-file-text-o"></i>Akses
                                Menu</a>
                        </li>
                    @endif
                    {{-- <li class="menu-item-has-children dropdown"> --}}
                    {{-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Data</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-file-text-o"></i><a href="{{ url('master/gardu_induk') }}">&nbsp;Gardu Induk</a></li> --}}
                    {{-- <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Badges</a></li>
                        <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tabs</a></li>

                        <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Cards</a></li>
                        <li><i class="fa fa-exclamation-triangle"></i><a href="ui-alerts.html">Alerts</a></li>
                        <li><i class="fa fa-spinner"></i><a href="ui-progressbar.html">Progress Bars</a></li>
                        <li><i class="fa fa-fire"></i><a href="ui-modals.html">Modals</a></li>
                        <li><i class="fa fa-book"></i><a href="ui-switches.html">Switches</a></li>
                        <li><i class="fa fa-th"></i><a href="ui-grids.html">Grids</a></li>
                        <li><i class="fa fa-file-word-o"></i><a href="ui-typgraphy.html">Typography</a></li> --}}
                    {{-- </ul>
                </li> --}}
                    {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tables</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                        <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                    </ul>
                </li> --}}
                    {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Forms</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                    </ul>
                </li> --}}

                    {{-- <li class="menu-title">Icons</li> --}}
                    <!-- /.menu-title -->

                    {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">Font Awesome</a>
                        </li>
                        <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">Themefy Icons</a></li>
                    </ul>
                </li>
                <li>
                    <a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets </a>
                </li> --}}
                    {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                        <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                    </ul>
                </li>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>
                        <li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a></li>
                    </ul>
                </li>
                <li class="menu-title">Extras</li><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                        <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                    </ul>
                </li> --}}
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>
<!-- /#left-panel -->
