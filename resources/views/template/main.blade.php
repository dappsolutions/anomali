<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title_top }}</title>
    <!-- add icon link -->
    <link rel="icon" href="{{ asset('assets/images/logo-login-modified.png') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('assets/css/normalize/normalize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap4/bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/fontawesome/fontawesome.css') }}"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/themefont/themefont.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/stroke/stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flagicon/flagicon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles/style.css') }}">
    <link href="{{ asset('assets/css/chartmin/chartmin.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/jqvmap/jqvmap.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/wheather/wheather.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/fullcalender/fullcalender.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/css/datatable/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loader/loader.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/template.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('assets/js/jquery3/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/dblocal/pounchdb.js') }}"></script>
    <script src="{{ asset('assets/js/popper/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap4/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootbox/bootbox.js') }}"></script>
    <script src="{{ asset('assets/js/jqueryhighligt/jqueryhighligt.js') }}"></script>
    <script src="{{ asset('assets/js/jquerymatchheight/jquery.matchheight.js') }}"></script>
    <script src="{{ asset('assets/js/utils/url.js') }}"></script>
    <script src="{{ asset('assets/js/utils/message.js') }}"></script>
    <script src="{{ asset('assets/js/utils/validation.js') }}"></script>
    <script src="{{ asset('assets/js/utils/database.js') }}"></script>
    <script src="{{ asset('assets/js/template.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>


    <script src="{{ asset('assets/js/dblocal/pounchdb.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/js/fullcalender/fullcalender.js') }}"></script>
    <script src="{{ asset('assets/js/init/fullcalendar-init.js') }}"></script>

    <style>
        #weatherWidget .currentDesc {
            color: #ffffff !important;
        }

        .traffic-chart {
            min-height: 335px;
        }

        #flotPie1 {
            height: 150px;
        }

        #flotPie1 td {
            padding: 3px;
        }

        #flotPie1 table {
            top: 20px !important;
            right: -10px !important;
        }

        .chart-container {
            display: table;
            min-width: 270px;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        #flotLine5 {
            height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }

        #cellPaiChart {
            height: 160px;
        }

    </style>

    @if (isset($header_data))
        @php
            $version = str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz');
        @endphp
        @foreach ($header_data as $key => $v_head)
            @php
                $data_key = explode('-', $key);
            @endphp
            @if ($data_key[0] == 'css')
                <link rel="stylesheet" href="{{ $v_head }}?v={{ $version }}">
            @else
                <script src="{{ $v_head }}?v={{ $version }}"></script>
            @endif
        @endforeach
    @endif

</head>

<body>
    @include('template.leftside')
    <div class="loader"></div>
    <div id="right-panel" class="right-panel">
        @include('template.header')

        @include('template.breadcumb')
        <!-- Content -->
        <div class="content">
            <div class="animated fadeIn">
                {!! isset($view_file) ? $view_file : '' !!}
            </div>
        </div>
        <!-- Content -->
        @include('template.footer')
    </div>
</body>

</html>
