<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="Departemen.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Departemen</h3>
                        </div>
                        <hr>
                        <form>     
                            <div class="form-group has-success">
                                <label for="nama_departemen" class="control-label mb-1">Nama Departemen</label>
                                <input id="nama_departemen" name="nama_departemen" type="text" class="form-control required"
                                    error="nama_departemen" value="{{ isset($nama_departemen) ? $nama_departemen : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="nama_departemen"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div>
                                <button id="save-button" onclick="Departemen.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
