<div class="row">
    <div class="col-md-12">
        <div class="" style="padding: 16px;">
            <div class="row form-group">
                <div class="col-md-3">
                    <label class=" form-control-label">Pilih Kondisi</label>
                </div>
                <div class="col-md-9">
                    <div class="form-check">
                        <div class="radio">
                            <label for="rad-kondisi" class="form-check-label ">
                                <input type="radio" id="rad-kondisi" name="rad-kondisi" value="option1"
                                    class="form-check-input">Need Repair
                            </label>
                        </div>
                        <div class="radio">
                            <label for="rad-kondisi" class="form-check-label ">
                                <input type="radio" id="rad-kondisi" name="rad-kondisi" value="option2"
                                    class="form-check-input">Need Attention
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label class=" form-control-label">Uraian Kondisi</label>
                </div>
                <div class="col-md-6">
                    <textarea name="" id="uraian" rows="10" class="form-control required" error="Uraian"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
