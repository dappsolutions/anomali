<div class="row">
    <div class="col-md-12">
        <div class="" style="padding: 16px;">
            <div class="row form-group">
                <div class="col-md-3">
                    <label class=" form-control-label">Pilih Phasa</label>
                </div>
                <div class="col-md-9">
                    <div class="form-check">
                        <div class="radio">
                            <label for="rad-phasa" class="form-check-label ">
                                <input type="radio" id="rad-phasa" name="rad-phasa" value="option1"
                                    class="form-check-input">R
                            </label>
                        </div>
                        <div class="radio">
                            <label for="rad-phasa" class="form-check-label ">
                                <input type="radio" id="rad-phasa" name="rad-phasa" value="option2"
                                    class="form-check-input">S
                            </label>
                        </div>
                        <div class="radio">
                            <label for="rad-phasa" class="form-check-label ">
                                <input type="radio" id="rad-phasa" name="rad-phasa" value="option3"
                                    class="form-check-input">T
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
