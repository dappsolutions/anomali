<div class="row">
    <div class="col-md-12">
        <div class="" style="padding: 16px;">
            <div class="row form-group">
                <div class="col-md-2">
                    <label class=" form-control-label">Foto</label>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="upload-foto" name="upload-foto" placeholder="Upload Foto"
                                class="form-control">
                            <div class="input-group-btn"><button class="btn btn-info"
                                    onclick="Anomali.takeFoto(this, event)">Upload</button></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" id="deskripsi" placeholder="Deskripsi">
                    </div>
                </div>
                <div class="col-md-2 text-right" id="action">
                    <button class="btn btn-outline-primary" onclick="Anomali.addFoto(this)">+</button>
                </div>
            </div>
        </div>
    </div>
</div>
