<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="Anomali.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-12">
        <div class="default-tab">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active show" id="nav-lokasi-tab" data-toggle="tab" href="#nav-lokasi"
                        role="tab" aria-controls="nav-lokasi" aria-selected="true"><label for="" class="label-circle">1</label> Lokasi Anomali</a>
                    <a class="nav-item nav-link" id="nav-tanggal-tab" data-toggle="tab" href="#nav-tanggal" role="tab"
                        aria-controls="nav-tanggal" aria-selected="false"><label for="" class="label-circle">2</label> Tanggal Anomali</a>
                    <a class="nav-item nav-link" id="nav-problem-tab" data-toggle="tab" href="#nav-problem" role="tab"
                        aria-controls="nav-problem" aria-selected="false"><label for="" class="label-circle">3</label> Problem & Peralatan</a>
                    <a class="nav-item nav-link" id="nav-phasa-tab" data-toggle="tab" href="#nav-phasa" role="tab"
                        aria-controls="nav-phasa" aria-selected="false"><label for="" class="label-circle">4</label> Phasa</a>
                    <a class="nav-item nav-link" id="nav-kondisi-tab" data-toggle="tab" href="#nav-kondisi" role="tab"
                        aria-controls="nav-kondisi" aria-selected="false"><label for="" class="label-circle">5</label> Kondisi Anomali</a>
                    <a class="nav-item nav-link" id="nav-foto-tab" data-toggle="tab" href="#nav-foto" role="tab"
                        aria-controls="nav-foto" aria-selected="false"><label for="" class="label-circle">6</label> Foto Anomali</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade active show" id="nav-lokasi" role="tabpanel" aria-labelledby="nav-lokasi-tab">
                    @include('anomali.form.lokasi')
                </div>
                <div class="tab-pane fade" id="nav-tanggal" role="tabpanel" aria-labelledby="nav-tanggal-tab">
                    @include('anomali.form.date')
                </div>
                <div class="tab-pane fade" id="nav-problem" role="tabpanel" aria-labelledby="nav-problem-tab">
                    @include('anomali.form.problem')
                </div>
                <div class="tab-pane fade" id="nav-phasa" role="tabpanel" aria-labelledby="nav-phasa-tab">
                    @include('anomali.form.phasa')
                </div>
                <div class="tab-pane fade" id="nav-kondisi" role="tabpanel" aria-labelledby="nav-kondisi-tab">
                    @include('anomali.form.kondisi')
                </div>
                <div class="tab-pane fade" id="nav-foto" role="tabpanel" aria-labelledby="nav-foto-tab">
                    @include('anomali.form.foto')
                </div>
            </div>

        </div>
    </div>
</div>

{{-- <div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Critical Bay</h3>
                        </div>
                        <hr>
                        <form>
                            <div class="form-group has-success">
                                <label for="bay" class="control-label mb-1">Bay</label>
                                <select name="bay" id="bay" class="form-control required" error="Bay">
                                    @foreach ($list_bay as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($bay))
                                            @if ($bay == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">
                                            {{ $item['nama_bay'] }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="upt"
                                    data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group has-success">
                                <label for="apb_risk" class="control-label mb-1">Apd Risk</label>
                                <input id="apb_risk" name="apb_risk" type="text" class="form-control required"
                                    error="apb_risk" value="{{ isset($apb_risk) ? $apb_risk : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="apb_risk"
                                    data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group has-success">
                                <label for="distribution_risk" class="control-label mb-1">Distribution Risk</label>
                                <input id="distribution_risk" name="distribution_risk" type="text"
                                    class="form-control required" error="distribution_risk"
                                    value="{{ isset($distribution_risk) ? $distribution_risk : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="distribution_risk"
                                    data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group has-success">
                                <label for="highest_risk" class="control-label mb-1">Highest Risk</label>
                                <input id="highest_risk" name="highest_risk" type="text" class="form-control required"
                                    error="highest_risk" value="{{ isset($highest_risk) ? $highest_risk : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="highest_risk"
                                    data-valmsg-replace="true"></span>
                            </div>
                            <div>
                                <button id="save-button" onclick="Anomali.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
