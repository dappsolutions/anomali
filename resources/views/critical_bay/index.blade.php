<div class="row">
    <div class="col-md-12">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Daftar Data</strong>
                <br>
                <small><span class="btn btn-outline-primary btn-sm float-right mt-1" onclick="CriticalBay.add(this)">Tambah
                        Baru</span></small>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-data-gardu-induk" class="table-thin table-striped table-bordered">
                        <thead>
                            <tr class="bg-yellow-apps-bold">
                                <th class="th-padd">No</th>
                                <th class="th-padd">Nama Bay</th>
                                <th class="th-padd">Apd Risk</th>
                                <th class="th-padd">Distribution Risk</th>
                                <th class="th-padd">Highest Risk</th>
                                <th class="text-center th-padd">-</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6" class="text-center td-padd">Tidak ada data ditemukan</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
