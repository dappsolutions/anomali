<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="CriticalBay.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Critical Bay</h3>
                        </div>
                        <hr>
                        <form>                           
                            <div class="form-group has-success">                                
                                <label for="bay" class="control-label mb-1">Bay</label>
                                <select name="bay" id="bay" class="form-control required" error="Bay">
                                    @foreach ($list_bay as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($bay))
                                            @if ($bay == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif                                            
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_bay'] }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="upt"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="apb_risk" class="control-label mb-1">Apd Risk</label>
                                <input id="apb_risk" name="apb_risk" type="text" class="form-control required"
                                    error="apb_risk" value="{{ isset($apb_risk) ? $apb_risk : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="apb_risk"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="distribution_risk" class="control-label mb-1">Distribution Risk</label>
                                <input id="distribution_risk" name="distribution_risk" type="text" class="form-control required"
                                    error="distribution_risk" value="{{ isset($distribution_risk) ? $distribution_risk : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="distribution_risk"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="highest_risk" class="control-label mb-1">Highest Risk</label>
                                <input id="highest_risk" name="highest_risk" type="text" class="form-control required"
                                    error="highest_risk" value="{{ isset($highest_risk) ? $highest_risk : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="highest_risk"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div>
                                <button id="save-button" onclick="CriticalBay.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
