<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="Pegawai.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Pegawai</h3>
                        </div>
                        <hr>
                        <form>                           
                            <div class="form-group has-success">                                
                                <label for="upt" class="control-label mb-1">UPT</label>
                                <select name="upt" id="upt" class="form-control required" error="UPT">
                                    @foreach ($list_upt as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($upt))
                                            @if ($upt == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif                                            
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_upt'] }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="upt"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">                                
                                <label for="departemen" class="control-label mb-1">Departemen</label>
                                <select name="departemen" id="departemen" class="form-control required" error="departemen">
                                    @foreach ($list_dept as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($departemen))
                                            @if ($departemen == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif                                            
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_departemen'] }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="departemen"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">                                
                                <label for="jabatan" class="control-label mb-1">Jabatan</label>
                                <select name="jabatan" id="jabatan" class="form-control required" error="Jabatan">
                                    @foreach ($list_jabatan as $item)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if (isset($jabatan))
                                            @if ($jabatan == $item['id'])
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif                                            
                                        @endif
                                        <option {{ $selected }} value="{{ $item['id'] }}">{{ $item['nama_jabatan'] }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block field-validation-valid" data-valmsg-for="jabatan"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="nip" class="control-label mb-1">NIP Pegawai</label>
                                <input id="nip" name="nip" type="text" class="form-control required"
                                    error="nip" value="{{ isset($nip) ? $nip : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="nip"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div class="form-group has-success">
                                <label for="nama_pegawai" class="control-label mb-1">Nama Pegawai</label>
                                <input id="nama_pegawai" name="nama_pegawai" type="text" class="form-control required"
                                    error="nama_pegawai" value="{{ isset($nama_pegawai) ? $nama_pegawai : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="nama_pegawai"
                                    data-valmsg-replace="true"></span>
                            </div> 
                            <div>
                                <button id="save-button" onclick="Pegawai.submit(this, event)" type="submit"
                                    class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                    <span id="save-button-amount">Proses</span>
                                    <span id="save-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
