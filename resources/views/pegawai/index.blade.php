@if (isset($akses->pegawai))
    <input type="hidden" name="" id="akses-update" value="{{ $akses->pegawai->update }}">
    <input type="hidden" name="" id="akses-delete" value="{{ $akses->pegawai->delete }}">
    <div class="row">
        <div class="col-md-12">
            <div class="card border">
                <div class="card-header">
                    <strong class="card-title">Daftar Data</strong>
                    @if ($akses->pegawai->create == 1)
                        <br>
                        <small><span class="btn btn-outline-primary btn-sm float-right mt-1"
                                onclick="Pegawai.add(this)">Tambah
                                Baru</span></small>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table-data-gardu-induk" class="table-thin table-striped table-bordered">
                            <thead>
                                <tr class="bg-yellow-apps-bold">
                                    <th class="th-padd">No</th>
                                    <th class="th-padd">Unit</th>
                                    <th class="th-padd">Nip</th>
                                    <th class="th-padd">Nama Pegawai</th>
                                    <th class="text-center th-padd">-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="text-center td-padd">Tidak ada data ditemukan</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    @include('informasi.index')
@endif
