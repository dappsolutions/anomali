<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ela Admin - HTML5 Admin Template</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="{{ asset('assets/css/normalize/normalize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/themefont/themefont.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/stroke/stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flagicon/flagicon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loader/loader.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/template.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <script src="{{ asset('assets/js/jquery3/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/utils/url.js') }}"></script>
    <script src="{{ asset('assets/js/utils/message.js') }}"></script>
    <script src="{{ asset('assets/js/utils/validation.js') }}"></script>
    <script src="{{ asset('assets/js/popper/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap4/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jqueryhighligt/jqueryhighligt.js') }}"></script>
    <script src="{{ asset('assets/js/dblocal/pounchdb.js') }}"></script>
    <script src="{{ asset('assets/js/utils/database.js') }}"></script>
    <script src="{{ asset('assets/js/controllers/login.js') }}"></script>
</head>

<body class="">
    <div class="loader"></div>
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="<?php echo url('/login') ?>">
                        <img class="align-content" src="{{ asset('assets/images/logo-login.svg') }}" alt="" style="margin-left: 24px;">
                        <br>                        
                        <center>Sistem Manajemen Anomali RKAU Terintegrasi</center>
                    </a>                    
                </div>
                <div class="login-form">
                    <div class="message"></div>
                    <form>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="email" class="form-control required" id="username" error="Username" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control required" error="Password" id="password" placeholder="Password">
                        </div>
                        {{-- <div class="checkbox">                           
                            <label class="pull-right">
                                <a href="#">Forgotten Password?</a>
                            </label>
                        </div> --}}
                        <button type="submit" onclick="Login.signIn(this, event)" class="btn btn-primary-thin btn-flat m-b-30 m-t-30">Sign in</button>
                        {{-- <div class="register-link m-t-15 text-center">
                            <p>Don't have account ? <a href="#"> Sign Up Here</a></p>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>    
</body>

</html>
