<input type="hidden" id="id" value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12 text-right">
        <button onclick="AksesMenu.back()" class="btn btn-outline-primary btn-sm">Kembali</button>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Form</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Akses Menu</h3>
                        </div>
                        <hr>
                        <form>
                            <div class="form-group has-success">
                                <label for="hak_akses" class="control-label mb-1">Hak Akses</label>
                                <input readonly id="hak_akses" name="hak_akses" type="text"
                                    class="form-control required" error="hak_akses"
                                    value="{{ isset($hak_akses) ? $hak_akses : '' }}">
                                <span class="help-block field-validation-valid" data-valmsg-for="hak_akses"
                                    data-valmsg-replace="true"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card border">
            <div class="card-header">
                <strong class="card-title">Menu Aplikasi</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="content-dept">
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center">Menu</h3>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table-thin table-striped table-bordered" id="table-menu-akses">
                                        <thead>
                                            <tr class="bg-yellow-apps-bold">
                                                <th class="th-padd">No</th>
                                                <th class="th-padd">Nama Menu</th>
                                                <th class="th-padd">Simpan</th>
                                                <th class="th-padd">Ubah</th>
                                                <th class="th-padd">Hapus</th>
                                                <th class="th-padd"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($list_akses as $item)
                                                <tr class="input" master_id='{{ $item->id }}'
                                                    hak_id="{{ $item->hak_akses != '' ? $item->hak_id : '' }}">
                                                    <td class="td-padd">{{ $no++ }}</td>
                                                    <td class="td-padd">{{ $item->nama_menu }}</td>
                                                    <td class="td-padd text-center">
                                                        <input type="checkbox" id="create"
                                                            {{ $item->create == 1 && $item->hak_akses != '' ? 'checked' : '' }}>
                                                    </td>
                                                    <td class="td-padd text-center">
                                                        <input type="checkbox" id="update"
                                                            {{ $item->update == 1 && $item->hak_akses != '' ? 'checked' : '' }}>
                                                    </td>
                                                    <td class="td-padd text-center">
                                                        <input type="checkbox" id="delete"
                                                            {{ $item->delete == 1 && $item->hak_akses != '' ? 'checked' : '' }}>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <button id="save-button" onclick="AksesMenu.submit(this, event)" type="submit"
                                        class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-arrow-right fa-lg"></i>&nbsp;
                                        <span id="save-button-amount">Proses</span>
                                        <span id="save-button-sending" style="display:none;">Sending…</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
