<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Apps</title>
    <meta name="description" content="Aplikasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('assets/css/fontawesome/all.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/fontawesome/material.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0"
        href="{{ asset('assets/css/styles/shards-dashboards.1.1.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles/extras.1.1.0.min.css') }}">
    <script src="{{ asset('assets/js/jquery3/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper/popper.min.js') }}" ></script>
    <script src="{{ asset('assets/js/bootstrap4/bootstrap.min.js') }}"></script>

    @if (isset($header_data))
        @foreach ($header_data as $key => $v_head)
            @php
                $data_key = explode('-', $key);
            @endphp
            @if ($data_key[0] == 'css')
                <link rel="stylesheet" href="">
            @else
                <script src="{{ $v_head }}"></script>
            @endif
        @endforeach
    @endif
</head>
</head>

<body class="h-100">
    <div class="container-fluid">
        <div class="row">
            @include('template.sidebar')
            <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                @include('template.navbar')
            </main>
        </div>        
    </div>
</body>

</html>
