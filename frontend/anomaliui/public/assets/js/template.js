Template = {
    logout:(elm, e)=>{
        e.preventDefault();
        Database.removeDoc('user_login');
        Database.removeDoc('token');
        setTimeout(function(){
            window.location.href = $(elm).attr('href');
        }, 1500);
    }
};

$(function () {
});
