let mymap;
let fg;
let markerClusters;
let map_marker_type = 0;

function markerClickOperation(e) {
    // console.log('e', e);
    let html = `Data Gardu Induk`;
    bootbox.dialog({
        message: html
    });
}

let Dashboard = {
    tokenApi: 'pk.eyJ1IjoiZGFwcHNvbHV0aW9ucyIsImEiOiJja2RiazdvZDQwY2w4MnNxdDR0ejVrcTB5In0.YCMYhheEcT0eB0aXmVd28w',
    getLocation: () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(Dashboard.showPosition);
        } else {
            ALERT("Tutup browser terlebih dahulu, kemudian login ulang, nyalakan LOKASI HP anda");
        }
    },

    showPosition: (position) => {
        // console.log('position', position);
        let lat = position.coords.latitude;
        let lng = position.coords.longitude;
        // mymap = L.map('map').setView([-7.524723610223271, 112.52574034503509], 13);
        mymap = L.map('map').setView([lat, lng], 13);
        mymap.on('moveend', function () {
            if (map_marker_type == 3 && fg) {
                mymap.addLayer(fg);
            }
        });
        L.tileLayer(
            `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${Dashboard.tokenApi}`, {
                maxZoom: 18,
                attribution: 'Lokasi Gardu Induk PT PLN Transmisi Jawa Bagian Timur dan Bali',
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
            }).addTo(mymap);
        // L.marker([-7.524723610223271, 112.52574034503509]).addTo(mymap)
        L.marker([lat, lng]).addTo(mymap)
            .bindPopup("<b>Gardu Induk</b><br />MOJOSARI.").openPopup()
            .on('click', markerClickOperation);
    },

    removeAllMarkers: () => {
        if (fg) {
            fg.clearLayers();
            mymap.removeLayer(fg);
        }
        fg = null;
        markerClusters = null;
    },

    markerClickOperation: (e) => {
        console.log(e);
    },

    // setMarker: (data) => {
    //     markerClusters = L.markerClusterGroup();
    //     markerClusters.on('clusterclick', function (a) {
    //         a.layer.zoomToBounds({
    //             padding: [20, 20]
    //         });
    //     });
    //     fg = L.featureGroup();

    //     $.each(data.data, function (index, value) {
    //       // console.log('value', value);
    //         let location_latitude = value.location_latitude;
    //         let location_longitude = value.location_longitude;
    //         let marker = L.marker([parseFloat(location_latitude), parseFloat(location_longitude)], {
    //                 customId: value.id
    //             })
    //             .bindPopup('<b>' + value.nama_pasar + '</b>')
    //             .on('click', Dasboard.markerClickOperation(this));
    //         // .addTo(fg);

    //         markerClusters.addLayer(marker);
    //     });
    //     fg.addLayer(markerClusters);
    //     mymap.addLayer(fg);

    //     setTimeout(function () {
    //         // mymap.fitBounds(fg.getBounds());
    //         if (fg) {
    //             mymap.flyToBounds(fg.getBounds());
    //         }
    //     }, 1000);
    // }
    setFSelect: () => {
        $('#upt').fSelect({
            placeholder: '+ UPT',
        });
    },

    getFilterUpt:(elm)=>{
        console.log($(elm).val());
    }
};

$(function () {
    Dashboard.removeAllMarkers();
    Dashboard.getLocation();
    Dashboard.setFSelect();
    // var map = L.map('map').setView([51.505, -0.09], 13);
    // let accessToken = `pk.eyJ1IjoiZGFwcHNvbHV0aW9ucyIsImEiOiJja2RiazdvZDQwY2w4MnNxdDR0ejVrcTB5In0.YCMYhheEcT0eB0aXmVd28w`;
    // L.tileLayer(`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${accessToken}`, {
    //     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    //     maxZoom: 18,
    //     id: 'mapbox/streets-v11',
    //     tileSize: 512,
    //     zoomOffset: -1,
    //     accessToken: `${accessToken}`
    // }).addTo(map);
    // var marker = L.marker([51.5, -0.09]).addTo(map);
});
