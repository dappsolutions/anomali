let Anomali = {
    module: () => {
        return "anomali/anomali";
    },

    moduleApi: () => {
        return `api/${Anomali.module()}`;
    },

    add: () => {
        window.location.href = url.base_url(Anomali.module()) + "add";
    },

    back: () => {
        window.location.href = url.base_url(Anomali.module()) + "index";
    },

    getData: async () => {
        let tableData = $('table#table-data');
        // let tokenApi = await Anomali.getToken();

        let db = Database.init();
        db.get('token').then(function (doc) {
            let tokenApi = doc.title;
            // console.log('tokenApi', tokenApi);
            return tableData.DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "autoWidth": false,
                "order": [
                    [0, 'asc']
                ],
                "aLengthMenu": [
                    [25, 50, 100],
                    [25, 50, 100]
                ],
                "ajax": {
                    "url": url.base_url(Anomali.moduleApi()) + `getData`,
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': `'${tokenApi}'`
                    }
                },
                "deferRender": true,
                "createdRow": function (row, data, dataIndex) {
                    // console.log('row', $(row));
                },
                "columnDefs": [{
                    "targets": 5,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass('text-center');
                        $(td).addClass('action');
                    }
                }],
                "columns": [{
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        "data": "no_document",
                    },                   
                    {
                        "data": "id",
                        "render": function (data, type, row) {
                            var html = "";
                            html += `<a href='${url.base_url(Anomali.module())}ubah/${data}' data_id="${row.id}" class="item_edit" style="cursor: pointer;"><img src="${url.base_url('assets/images')}Form.svg"></a>`;
                            html += `<a href="javascript:;" data_id="${row.id}" class="item_edit" style="cursor: pointer;" onclick="Anomali.delete(this, event)"><img src="${url.base_url('assets/images')}DeleteOutlined.svg"></a>`;
                            return html;
                        }
                    },
                ]
            });
        });
    },

    delete:(elm, e)=>{
        e.preventDefault();
        let data_id = $(elm).attr('data_id');
        let html = `<div class="row">
        <div class="col-md-12">
            <p>Apakah anda yakin akan menghapus data ini ?</p>
        </div>
        <div class="col-md-12 text-center">
            <br/>
            <button class="btn btn-primary btn-sm" onclick="Anomali.deleteConfirm(this, '${data_id}')">Ya</button>
            <button class="btn btn-sm" onclick="message.closeDialog()">Tidak</button>
        </div>
        </div>`;

        bootbox.dialog({
            message : html
        });
    },

    deleteConfirm:(elm, id)=>{
        let params = {};
        params.id = id;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: params,
            url: url.base_url(Anomali.moduleApi()) + "delete",

            beforeSend:()=>{
                message.loadingProses('Proses Hapus Data');
            },

            error: function () {
                message.closeLoading();
                toastr.error("Gagal");
            },

            success: function (resp) {
                message.closeLoading();
                if(resp.is_valid){
                    toastr.success('Data Berhasil Dihapus');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }else{
                    toastr.error('Data Gagal Dihapus ', resp.message);
                }
            }
        });
    },

    getPostData: () => {
        let data = {
            'id': $('input#id').val(),
            'bay': $('#bay').val(),
            'apb_risk': $('input#apb_risk').val(),
            'distribution_risk': $('input#distribution_risk').val(),
            'highest_risk': $('input#highest_risk').val(),
        };
        return data;
    },

    submit: (elm, e) => {
        e.preventDefault();
        let params = Anomali.getPostData();

        if (validation.run()) {
            let db = Database.init();
            db.get('token').then(function (doc) {
                params.tokenApi = doc.title;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: params,
                    url: url.base_url(Anomali.moduleApi()) + "submit",
                    beforeSend: () => {
                        message.loadingProses('Proses Simpan Data...');
                    },
                    error: function () {
                        message.closeLoading();
                        toastr.error("Gagal");
                    },

                    success: function (resp) {
                        message.closeLoading();
                        if (resp.is_valid) {
                            toastr.success('Data Berhasil Disimpan');
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        } else {
                            bootbox.dialog({
                                message: resp.message
                            });
                        }
                    }
                });
            });
        }
    },

    takeFoto: (elm, e) => {
        e.preventDefault();
        var uploader = $('<input type="file" accept="image/*;capture=camera" />');
        var src_foto = $(elm).closest('div.row').find("#source-foto");
        uploader.click();

        uploader.on("change", function () {
            var reader = new FileReader();
            reader.onload = function (event) {
                var files = $(uploader).get(0).files[0];
                filename = files.name;
                var data_from_file = filename.split(".");
                var type_file = $.trim(data_from_file[data_from_file.length - 1]);
                $(elm).closest('div.row').find('input#upload-foto').val(filename);

                var data = event.target.result;
                src_foto.attr("src", data);

                // var data_source = data.split(';');
                // console.log(data_source[1].length);
                src_foto.removeClass("hide");
            };

            reader.readAsDataURL(uploader[0].files[0]);
        });
    },

    addFoto:(elm)=>{
        let divRow = $(elm).closest('div.row');
        let newDivRow = divRow.clone();
        newDivRow.find('div#action').html(`<button class="btn btn-outline-danger" onclick="Anomali.removeFoto(this)">-</button>`);
        divRow.after(newDivRow);
    },

    removeFoto:(elm)=>{
        $(elm).closest('div.row').remove();
    }
};

$(function () {
    Anomali.getData();
});
