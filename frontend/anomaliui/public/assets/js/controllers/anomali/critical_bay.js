let CriticalBay = {
    module: () => {
        return "anomali/critical_bay";
    },

    moduleApi: () => {
        return `api/${CriticalBay.module()}`;
    },

    add: () => {
        window.location.href = url.base_url(CriticalBay.module()) + "add";
    },

    back: () => {
        window.location.href = url.base_url(CriticalBay.module()) + "index";
    },

    getData: async () => {
        let tableData = $('table#table-data-gardu-induk');
        // let tokenApi = await CriticalBay.getToken();

        let db = Database.init();
        db.get('token').then(function (doc) {
            let tokenApi = doc.title;
            // console.log('tokenApi', tokenApi);
            return tableData.DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "autoWidth": false,
                "order": [
                    [0, 'asc']
                ],
                "aLengthMenu": [
                    [25, 50, 100],
                    [25, 50, 100]
                ],
                "ajax": {
                    "url": url.base_url(CriticalBay.moduleApi()) + `getData`,
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': `'${tokenApi}'`
                    }
                },
                "deferRender": true,
                "createdRow": function (row, data, dataIndex) {
                    // console.log('row', $(row));
                },
                "columnDefs": [{
                    "targets": 5,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass('text-center');
                        $(td).addClass('action');
                    }
                }],
                "columns": [{
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        "data": "nama_bay",
                    },
                    {
                        "data": "apb_risk",
                    },
                    {
                        "data": "distribution_risk",
                    },
                    {
                        "data": "highest_risk",
                    },
                    {
                        "data": "id",
                        "render": function (data, type, row) {
                            var html = "";
                            html += `<a href='${url.base_url(CriticalBay.module())}ubah/${data}' data_id="${row.id}" class="item_edit" style="cursor: pointer;"><img src="${url.base_url('assets/images')}Form.svg"></a>`;
                            html += `<a href="javascript:;" data_id="${row.id}" class="item_edit" style="cursor: pointer;" onclick="CriticalBay.delete(this, event)"><img src="${url.base_url('assets/images')}DeleteOutlined.svg"></a>`;
                            return html;
                        }
                    },
                ]
            });
        });
    },

    delete:(elm, e)=>{
        e.preventDefault();
        let data_id = $(elm).attr('data_id');
        let html = `<div class="row">
        <div class="col-md-12">
            <p>Apakah anda yakin akan menghapus data ini ?</p>
        </div>
        <div class="col-md-12 text-center">
            <br/>
            <button class="btn btn-primary btn-sm" onclick="CriticalBay.deleteConfirm(this, '${data_id}')">Ya</button>
            <button class="btn btn-sm" onclick="message.closeDialog()">Tidak</button>
        </div>
        </div>`;

        bootbox.dialog({
            message : html
        });
    },

    deleteConfirm:(elm, id)=>{
        let params = {};
        params.id = id;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: params,
            url: url.base_url(CriticalBay.moduleApi()) + "delete",

            beforeSend:()=>{
                message.loadingProses('Proses Hapus Data');
            },

            error: function () {
                message.closeLoading();
                toastr.error("Gagal");
            },

            success: function (resp) {
                message.closeLoading();
                if(resp.is_valid){
                    toastr.success('Data Berhasil Dihapus');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }else{
                    toastr.error('Data Gagal Dihapus ', resp.message);
                }
            }
        });
    },

    getPostData: () => {
        let data = {
            'id': $('input#id').val(),
            'bay': $('#bay').val(),
            'apb_risk': $('input#apb_risk').val(),
            'distribution_risk': $('input#distribution_risk').val(),
            'highest_risk': $('input#highest_risk').val(),
        };
        return data;
    },

    submit: (elm, e) => {
        e.preventDefault();
        let params = CriticalBay.getPostData();

        if (validation.run()) {
            let db = Database.init();
            db.get('token').then(function (doc) {
                params.tokenApi = doc.title;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: params,
                    url: url.base_url(CriticalBay.moduleApi()) + "submit",
                    beforeSend: () => {
                        message.loadingProses('Proses Simpan Data...');
                    },
                    error: function () {
                        message.closeLoading();
                        toastr.error("Gagal");
                    },

                    success: function (resp) {
                        message.closeLoading();
                        if (resp.is_valid) {
                            toastr.success('Data Berhasil Disimpan');
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        } else {
                            bootbox.dialog({
                                message: resp.message
                            });
                        }
                    }
                });
            });
        }
    },
};

$(function () {
    CriticalBay.getData();
});
