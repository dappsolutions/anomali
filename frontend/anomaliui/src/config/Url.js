
let UrlConfig = {
    BASE_URL : 'http://localhost:8000',
    BASE_URL_REACT : 'http://localhost:3000',
    BASE_URL_API : 'http://localhost:8000/api',
};

export default UrlConfig;