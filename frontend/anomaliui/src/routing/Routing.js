import {
    Routes,
    Route,
  } from "react-router-dom";
import Dashboard from "../dashboard/Dashboard";
import AddGarduInduk from "../gardu_induk/AddGarduInduk";
import GarduInduk from "../gardu_induk/GarduInduk";

function Routing(){
    return (
        <Routes>
        <Route path="/" element={<Dashboard />} />
        <Route path="dashboard" element={<Dashboard />} />
        <Route path="master/gardu_induk" element={<GarduInduk />} />
        <Route path="master/gardu_induk/add" element={<AddGarduInduk />} />
      </Routes>
    );
}

export default Routing;