import React, { useEffect } from 'react';
import $ from "jquery";
import 'datatables.net-bs4';
import UrlConfig from '../config/Url';
import {useNavigate} from 'react-router-dom';


function GarduInduk(){
    let navigate = useNavigate();
    
    useEffect(() => {
        let tableData = $('#table-data');
        tableData.DataTable({
            "processing": true,
                "serverSide": true,
                "ordering": true,
                "autoWidth": false,
                "order": [
                    [0, 'asc']
                ],
                "aLengthMenu": [
                    [25, 50, 100],
                    [25, 50, 100]
                ],
                "ajax": {
                    "url": `${UrlConfig.BASE_URL_API}/master/gardu_induk/getData`,
                    "type": "GET",
                    // "headers": {
                    //     "accept": "application/json",
                    //     "Access-Control-Allow-Origin":"*"
                    // }
                },
                "deferRender": true,
                "createdRow": function (row, data, dataIndex) {
                    // console.log('row', $(row));
                },
                "columnDefs": [{
                    "targets": 3,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass('text-center');
                        $(td).addClass('action');
                    }
                }],
                "columns": [{
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "nama_upt",
                },
                {
                    "data": "nama_gardu",
                },
                {
                    "data": "id",
                    "render": function (data, type, row) {
                        var html = "";
                        if($('input#akses-update').val() == 1){
                            html += `<a href='ubah/${data}' data_id="${row.id}" class="item_edit" style="cursor: pointer;"><img src="Form.svg"></a>`;
                        }
                        if($('input#akses-delete').val() == 1){
                            html += `<a href="javascript:;" data_id="${row.id}" class="item_edit" style="cursor: pointer;" onclick="GarduInduk.delete(this, event)"><img src="DeleteOutlined.svg"></a>`;
                        }                            
                        return html;
                    }
                },
            ]
        });
    });

    function add(e){
        // alert("Add Gardu");
        navigate('/master/gardu_induk/add');
    }

    return (
        <>
            <div className="row">
                <div className="col-md-12">
                    <div className="card border">
                        <div className="card-header">
                            <strong className="card-title">Daftar Data</strong>
                            <br />
                            <small><span className="btn btn-outline-primary btn-sm float-right mt-1" onClick={add}>Tambah
                                    Baru</span></small>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table id="table-data" className="table-thin table-striped table-bordered">
                                    <thead>
                                        <tr className="bg-yellow-apps-bold">
                                            <th className="th-padd">No</th>
                                            <th className="th-padd">Unit</th>
                                            <th className="th-padd">Gardu Induk</th>
                                            <th className="text-center th-padd">-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>2</td>
                                            <td>GI</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </>
    );
}

export default GarduInduk;