import { Button, Modal } from "bootstrap";
import { useState } from "react";

function TesModal(props){
    // const [show, setShow] = useState(false);

    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);

    return (
        <div id="content" className={props.classModal} role="dialog">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <h4 className="modal-title">Bagian heading modal</h4>
                    </div>
                    <div className="modal-body">
                        <p>bagian body modal.</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-default" data-dismiss="modal">Tutup Modal</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TesModal;