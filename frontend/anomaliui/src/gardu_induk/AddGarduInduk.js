import {Link} from 'react-router-dom';
import $ from 'jquery';
import UrlConfig from '../config/Url';
import React, { Component} from 'react';
import Message from '../config/message';
import TesModal from './modal/TesModal';

class AddGarduInduk extends Component{    
    ListUptPage = [];
    constructor(props){
        super(props);        
        this.state = {
            dataUpt: [],
            setShowModal: "modal fade"
        };
    }

    handleShow = () => this.setShow(true);
    handleClose = () => this.setShow(false);
    setShow = (show) => {
        this.setState({
            setShowModal: show == true ? "modal show" : "modal fade"            
        });

        console.log(this.state.setShowModal);
    }

    getListUpt(callback){
        let params = {};
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: params,
            url: UrlConfig.BASE_URL_API + '/master/gardu_induk/getListUpt',
            beforeSend: () => {
                Message.loadingProses("Proses ambil data ...");
            },
            error: function () {
                Message.closeLoading();
            },

            success: function (resp) { 
                Message.closeLoading();  
                // Bootbox.dialog({
                //     message: resp,
                // });
                let result = [];          
                resp.map(function(data){
                    let params = {
                        value : data.id,
                        label: data.nama_upt
                    };
                    result.push(params);
                });

                callback(result);
            }
        });
    }

    componentDidMount(){
        let self = this;
        this.getListUpt((params)=>{
            self.setState({
                dataUpt: params
            });
        });
    }

    render(){
        return (
            <>
                <input id="id" type="hidden"/>
                <div className="row">
                    <div className="col-md-12 text-right">
                        <button className="btn btn-outline-primary btn-sm" onClick={this.handleShow}>TES MODAL</button>
                        <Link to="/master/gardu_induk" className="btn btn-outline-primary btn-sm">Kembali</Link>
                    </div>
                </div>
                <hr/>
    
                <div className="row">
                    <div className="col-md-6">
                        <div className="card border">
                            <div className="card-header">
                                <strong className="card-title">Form</strong>
                            </div>
                            <div className="card-body">
                                <div id="content-dept">
                                    <div className="card-body">
                                        <div className="card-title">
                                            <h3 className="text-center">Gardu Induk</h3>
                                        </div>
                                        <hr/>
                                        <form>                           
                                            <div className="form-group has-success">                                
                                                <label htmlFor="upt" className="control-label mb-1">UPT</label>
                                                <select className='form-control required' error="UPT" id="upt">
                                                    {
                                                        this.state.dataUpt.map((option) => (
                                                            <option key={option.value} value={option.value}>{option.label}</option>
                                                        ))
                                                    }
                                                </select>
                                            </div> 
                                            <div className="form-group has-success">
                                                <label htmlFor="nama_gardu" className="control-label mb-1">Nama GI</label>
                                                <input id="nama_gardu" name="nama_gardu" type="text" className="form-control required"
                                                    error="nama_gardu" />
                                                <span className="help-block field-validation-valid"
                                                    data-valmsg-replace="true"></span>
                                            </div> 
                                            <div>
                                                <button id="save-button" type="submit"
                                                    className="btn btn-lg btn-info btn-block">
                                                    <i className="fa fa-arrow-right fa-lg"></i>&nbsp;
                                                    <span id="save-button-amount">Proses</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <TesModal classModal={this.state.setShowModal}></TesModal>
                {/* <Modal>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal title</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <p>Modal body text goes here.</p>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary">Close</Button>
                        <Button variant="primary">Save changes</Button>
                    </Modal.Footer>
                </Modal> */}
            </>
        );
    }
}

export default AddGarduInduk;