import React from "react";
import { Link } from "react-router-dom";
function LeftContent(){
    return (
        <aside id="left-panel" className="left-panel">
            <nav className="navbar navbar-expand-sm navbar-default">
                <div id="main-menu" className="main-menu collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                        <li className="active">
                            <Link to="/dashboard"><i className="menu-icon fa fa-laptop"></i>Dashboard</Link>
                        </li>
                        <li className="menu-title">Master</li>
                        <li className="">
                            <Link to="/master/gardu_induk"><i className="menu-icon fa fa-file-text-o"></i> Gardu Induk</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </aside>
    );
}

export default LeftContent;