import UrlConfig from "../config/Url";

function HeaderContent(){
    const Path = {
        logoUrl : UrlConfig.BASE_URL_REACT+"/assets/images/logo-login.png",
        logoUserUrl : UrlConfig.BASE_URL_REACT+"/assets/images/user.png",
    };

    function logout(e){
        e.preventDefault();
        alert("Logout");
    }

    return (
        <header id="header" className="header">
            <div className="top-left">
                <div className="navbar-header">
                    <a className="navbar-brand" href="dashboard">
                        <img src={Path.logoUrl} alt="Logo" />
                    </a>
                    <a className="navbar-brand hidden" href="/"><img src={Path.logoUrl} alt="Logo"/></a>
                    <a id="menuToggle" className="menutoggle"><i className="fa fa-bars"></i></a>
                </div>
            </div>
            <div className="top-right">
                <div className="header-menu">
                    <div className="user-area dropdown float-right">
                        <a href="#" className="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img className="user-avatar rounded-circle" src={Path.logoUserUrl} alt="User Avatar"/>
                        </a>
                        <div className="user-menu dropdown-menu">
                            <a className="nav-link" href="#" id="user_login"></a>
                            <a className="nav-link" href="/" onClick={logout}><i className="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default HeaderContent;

