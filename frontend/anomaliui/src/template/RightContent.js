import Routing from "../routing/Routing";
import BredCumbContent from "./BreadCumbContent";
import FooterContent from "./FooterContent";
import HeaderContent from "./HeaderContent";

function RightContent(){
    return (        
        <div id="right-panel" className="right-panel">
            <div className="loader"></div>
            <HeaderContent/>
            <BredCumbContent/>  
            <div className="content">
                <div className="animated fadeIn">
                    <Routing />                                      
                </div>
            </div>
            <FooterContent/>
        </div>
    );
}

export default RightContent;