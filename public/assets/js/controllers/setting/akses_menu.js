let AksesMenu = {
    module: () => {
        return "setting/akses_menu";
    },

    moduleApi: () => {
        return `api/${AksesMenu.module()}`;
    },

    add: () => {
        window.location.href = url.base_url(AksesMenu.module()) + "add";
    },

    back: () => {
        window.location.href = url.base_url(AksesMenu.module()) + "index";
    },

    getData: async () => {
        let tableData = $('table#table-data-gardu-induk');
        // let tokenApi = await AksesMenu.getToken();

        let db = Database.init();
        db.get('token').then(function (doc) {
            let tokenApi = doc.title;
            // console.log('tokenApi', tokenApi);
            return tableData.DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "autoWidth": false,
                "order": [
                    [0, 'asc']
                ],
                "aLengthMenu": [
                    [25, 50, 100],
                    [25, 50, 100]
                ],
                "ajax": {
                    "url": url.base_url(AksesMenu.moduleApi()) + `getData`,
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': `'${tokenApi}'`
                    }
                },
                "deferRender": true,
                "createdRow": function (row, data, dataIndex) {
                    // console.log('row', $(row));
                },
                "columnDefs": [{
                    "targets": 2,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass('text-center');
                        $(td).addClass('action');
                    }
                }],
                "columns": [{
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        "data": "hak_akses",
                        "render": function (data, type, row, meta) {
                            return data.toUpperCase();
                        }
                    },                    
                    {
                        "data": "id",
                        "render": function (data, type, row) {
                            var html = "";
                            html += `<a href='${url.base_url(AksesMenu.module())}ubah/${data}' data_id="${row.id}" class="item_edit" style="cursor: pointer;"><img src="${url.base_url('assets/images')}Form.svg"></a>`;
                            return html;
                        }
                    },
                ]
            });
        });
    },

    delete:(elm, e)=>{
        e.preventDefault();
        let data_id = $(elm).attr('data_id');
        let html = `<div class="row">
        <div class="col-md-12">
            <p>Apakah anda yakin akan menghapus data ini ?</p>
        </div>
        <div class="col-md-12 text-center">
            <br/>
            <button class="btn btn-primary btn-sm" onclick="AksesMenu.deleteConfirm(this, '${data_id}')">Ya</button>
            <button class="btn btn-sm" onclick="message.closeDialog()">Tidak</button>
        </div>
        </div>`;

        bootbox.dialog({
            message : html
        });
    },

    deleteConfirm:(elm, id)=>{
        let params = {};
        params.id = id;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: params,
            url: url.base_url(AksesMenu.moduleApi()) + "delete",

            beforeSend:()=>{
                message.loadingProses('Proses Hapus Data');
            },

            error: function () {
                message.closeLoading();
                toastr.error("Gagal");
            },

            success: function (resp) {
                message.closeLoading();
                if(resp.is_valid){
                    toastr.success('Data Berhasil Dihapus');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }else{
                    toastr.error('Data Gagal Dihapus ', resp.message);
                }
            }
        });
    },

    getPostItemMenu:()=>{
        let table = $('table#table-menu-akses').find('tbody').find('tr.input');
        let data = [];
        $.each(table, function(){
            let menu_id = $(this).attr('master_id');
            let hak_id = $(this).attr('hak_id');
            let create = $(this).find('input#create').is(':checked') ? 1 : 0;
            let update = $(this).find('input#update').is(':checked') ? 1 : 0;
            let deleted = $(this).find('input#delete').is(':checked') ? 1 : 0;
            let params = {};
            params.menu_id = menu_id;
            params.hak_id = hak_id;
            params.create = create;
            params.update = update;
            params.delete = deleted;   
            data.push(params);
        }); 

        return data;
    },

    getPostData: () => {
        let data = {
            'id': $('input#id').val(),
            'data_menu': AksesMenu.getPostItemMenu(),
        };
        return data;
    },

    submit: (elm, e) => {
        e.preventDefault();
        let params = AksesMenu.getPostData();

        if (validation.run()) {
            let db = Database.init();
            db.get('token').then(function (doc) {
                params.tokenApi = doc.title;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: params,
                    url: url.base_url(AksesMenu.moduleApi()) + "submit",
                    beforeSend: () => {
                        message.loadingProses('Proses Simpan Data...');
                    },
                    error: function () {
                        message.closeLoading();
                        toastr.error("Gagal");
                    },

                    success: function (resp) {
                        message.closeLoading();
                        if (resp.is_valid) {
                            toastr.success('Data Berhasil Disimpan');
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        } else {
                            bootbox.dialog({
                                message: resp.message
                            });
                        }
                    }
                });
            });
        }
    },
};

$(function () {
    AksesMenu.getData();
});
