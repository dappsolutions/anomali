<?php

namespace App\Http\Controllers\Master;

use App\Gardu_induk;
use App\Http\Controllers\Controller;
use App\Upt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class GarduIndukController extends Controller{
    public $akses_menu = [];
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->akses_menu = json_decode(Cache::store('file')->get('akses_menu'));
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/controllers/master/gardu_induk.js'),
        );
    }

    public function getTitleParent(){
        return "Master";
    }

    public function getTableName(){
        return "gardu_induk";
    }

    public function index(){
        $data['data'] = [];
        $data['akses'] = $this->akses_menu;
        $view = view('gardu_induk.index', $data);

        $put['title_content'] = 'Gardu Induk';
        $put['title_top'] = 'Gardu Induk';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function getListUpt(){
        $data = Upt::where('nama_upt', '!=', 'SUPERADMIN')->get()->toArray();
        return $data;
    }
    
    public function getListUptApi(){
        $data = Upt::where('nama_upt', '!=', 'SUPERADMIN')->get()->toArray();
        return json_encode($data);
    }

    public function add(){
        $data['list_upt'] = $this->getListUpt();
        $data['akses'] = $this->akses_menu;
        $view = view('gardu_induk.formadd', $data);

        $put['title_content'] = 'Form Gardu Induk';
        $put['title_top'] = 'Form Gardu Induk';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function getDetailData($id){
        $data = Gardu_induk::where('id', $id)->first();
        return $data;
    }

    public function ubah($id){
        $data = $this->getDetailData($id);
        $data['list_upt'] = $this->getListUpt();
        $data['akses'] = $this->akses_menu;
        $view = view('gardu_induk.formadd', $data);

        $put['title_content'] = 'Form Gardu Induk';
        $put['title_top'] = 'Form Gardu Induk';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }
    
    public function getData(){
        DB::enableQueryLog();
        $data['data'] = [];
        $data['recordsTotal'] = 0;
        $data['recordsFiltered'] = 0;
        $garduInduk = DB::table($this->getTableName().' as gi')
        ->select([
            'gi.*',
            'ut.nama_upt'
        ])
        ->join('upt as ut', 'ut.id', '=', 'gi.upt')
        ->where('gi.deleted', '=', '0');
        if(isset($_GET)){            
            $data['recordsTotal'] = $garduInduk->get()->count();
            if(isset($_GET['search']['value'])){
                $keyword = $_GET['search']['value'];
                $garduInduk->where(function($query) use ($keyword){
                    $query->where('gi.nama_gardu', 'LIKE', '%'.$keyword.'%');
                    $query->orWhere('ut.nama_upt', 'LIKE', '%'.$keyword.'%');
                });
            }
            if(isset($_GET['order'][0]['column'])){
                $garduInduk->orderBy('gi.id', $_GET['order'][0]['dir']);
            }
            $data['recordsFiltered'] = $garduInduk->get()->count();

            if(isset($_GET['length'])){
                $garduInduk->limit($_GET['length']);
            }
            if(isset($_GET['start'])){
                $garduInduk->offset($_GET['start']);
            }
        }
        $data['data'] = $garduInduk->get()->toArray();
        $data['draw'] = $_GET['draw'];
        $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        return json_encode($data);
    }

    public function submit(Request $request){
        // ini_set('memory_limit', -1);
        $data = $request->post();
        // echo '<pre>';
        // print_r($data);die;
        $user_id = Cache::store('file')->get('user_id');
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {
        
            if($data['id'] == ''){
                //insert data
                $ins = new Gardu_induk();
                $ins->upt = $data['upt'];
                $ins->nama_gardu = $data['nama_gardu'];
                $ins->deleted = 0;
                $ins->save();
            }else{
                DB::table($this->getTableName())->where('id', $data['id'])->update([
                    'upt' => $data['upt'],
                    'nama_gardu' => $data['nama_gardu'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }

    public function delete(Request $request){
        $data = $request->post();
        
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {        
            //update data
            DB::table($this->getTableName())->where('id', $data['id'])->update([
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }
}
