<?php

namespace App\Http\Controllers\Master;

use App\Bay;
use App\Gardu_induk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class BayController extends Controller{
    public $akses_menu = [];
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->akses_menu = json_decode(Cache::store('file')->get('akses_menu'));
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/controllers/master/bay.js'),
        );
    }

    public function getTitleParent(){
        return "Master";
    }

    public function getTableName(){
        return "bay";
    }

    public function index(){
        $data['data'] = [];
        $data['akses'] = $this->akses_menu;
        $view = view('bay.index', $data);

        $put['title_content'] = 'Bay';
        $put['title_top'] = 'Bay';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['akses'] = $this->akses_menu;
        $put['header_data'] = $this->getHeaderCss();
        return view('template.main', $put);
    }

    public function getListGarduInduk(){
        $data = Gardu_induk::where('deleted', '=', '0')->get()->toArray();
        return $data;
    }

    public function add(){
        $data['list_gardu'] = $this->getListGarduInduk();
        $data['akses'] = $this->akses_menu;
        $view = view('bay.formadd', $data);

        $put['title_content'] = 'Form Bay';
        $put['title_top'] = 'Form Bay';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function getDetailData($id){
        $data = Bay::where('id', $id)->first();
        return $data;
    }

    public function ubah($id){
        $data = $this->getDetailData($id);
        $data['list_gardu'] = $this->getListGarduInduk();
        $data['akses'] = $this->akses_menu;
        $view = view('bay.formadd', $data);

        $put['title_content'] = 'Form Bay';
        $put['title_top'] = 'Form Bay';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }
    
    public function getData(){
        DB::enableQueryLog();
        $data['data'] = [];
        $data['recordsTotal'] = 0;
        $data['recordsFiltered'] = 0;
        $garduInduk = DB::table($this->getTableName().' as b')
        ->select([
            'b.*',
            'gi.nama_gardu'
        ])
        ->join('gardu_induk as gi', 'gi.id', '=', 'b.gardu_induk')
        ->where('b.deleted', '=', '0');
        if(isset($_GET)){            
            $data['recordsTotal'] = $garduInduk->get()->count();
            if(isset($_GET['search']['value'])){
                $keyword = $_GET['search']['value'];
                $garduInduk->where(function($query) use ($keyword){
                    $query->where('gi.nama_gardu', 'LIKE', '%'.$keyword.'%');
                    $query->orWhere('b.nama_bay', 'LIKE', '%'.$keyword.'%');
                });
            }
            if(isset($_GET['order'][0]['column'])){
                $garduInduk->orderBy('b.id', $_GET['order'][0]['dir']);
            }
            $data['recordsFiltered'] = $garduInduk->get()->count();

            if(isset($_GET['length'])){
                $garduInduk->limit($_GET['length']);
            }
            if(isset($_GET['start'])){
                $garduInduk->offset($_GET['start']);
            }
        }
        $data['data'] = $garduInduk->get()->toArray();
        $data['draw'] = $_GET['draw'];
        $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        return json_encode($data);
    }

    public function submit(Request $request){
        // ini_set('memory_limit', -1);
        $data = $request->post();
        // echo '<pre>';
        // print_r($data);die;
        $user_id = Cache::store('file')->get('user_id');
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {
        
            if($data['id'] == ''){
                //insert data
                $ins = new Bay();
                $ins->gardu_induk = $data['gardu_induk'];
                $ins->nama_bay = $data['nama_bay'];
                $ins->deleted = 0;
                $ins->save();
            }else{
                DB::table($this->getTableName())->where('id', $data['id'])->update([
                    'gardu_induk' => $data['gardu_induk'],
                    'nama_bay' => $data['nama_bay'],
                    'deleted' => 0
                ]);
            }
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }

    public function delete(Request $request){
        $data = $request->post();
        
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {        
            //update data
            DB::table($this->getTableName())->where('id', $data['id'])->update([
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }
}
