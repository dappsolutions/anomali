<?php

namespace App\Http\Controllers\Master;

use App\Alat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AlatController extends Controller
{
    public $akses_menu = [];
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->akses_menu = json_decode(Cache::store('file')->get('akses_menu'));
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/controllers/master/alat.js'),
        );
    }

    public function getTitleParent(){
        return "Master";
    }

    public function getTableName(){
        return "alat";
    }

    public function index(){
        $data['data'] = [];
        $data['akses'] = $this->akses_menu;
        $view = view('alat.index', $data);

        $put['title_content'] = 'Alat';
        $put['title_top'] = 'Alat';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function add(){
        $data['akses'] = $this->akses_menu;
        $view = view('alat.formadd', $data);

        $put['title_content'] = 'Form Alat';
        $put['title_top'] = 'Form Alat';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function getDetailData($id){
        $data = Alat::where('id', $id)->first();
        return $data;
    }

    public function ubah($id){
        $data = $this->getDetailData($id);
        $data['akses'] = $this->akses_menu;
        $view = view('alat.formadd', $data);

        $put['title_content'] = 'Form Alat';
        $put['title_top'] = 'Form Alat';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }
    
    public function getData(){
        DB::enableQueryLog();
        $data['data'] = [];
        $data['recordsTotal'] = 0;
        $data['recordsFiltered'] = 0;
        $garduInduk = DB::table($this->getTableName().' as b')
        ->select([
            'b.*'
        ])
        ->where('b.deleted', '=', '0');
        if(isset($_GET)){            
            $data['recordsTotal'] = $garduInduk->get()->count();
            if(isset($_GET['search']['value'])){
                $keyword = $_GET['search']['value'];
                $garduInduk->where(function($query) use ($keyword){
                    $query->where('b.nama_alat', 'LIKE', '%'.$keyword.'%');
                });
            }
            if(isset($_GET['order'][0]['column'])){
                $garduInduk->orderBy('b.id', $_GET['order'][0]['dir']);
            }
            $data['recordsFiltered'] = $garduInduk->get()->count();

            if(isset($_GET['length'])){
                $garduInduk->limit($_GET['length']);
            }
            if(isset($_GET['start'])){
                $garduInduk->offset($_GET['start']);
            }
        }
        $data['data'] = $garduInduk->get()->toArray();
        $data['draw'] = $_GET['draw'];
        $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        return json_encode($data);
    }

    public function submit(Request $request){
        // ini_set('memory_limit', -1);
        $data = $request->post();
        // echo '<pre>';
        // print_r($data);die;
        $user_id = Cache::store('file')->get('user_id');
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {
        
            if($data['id'] == ''){
                //insert data
                $ins = new Alat();
                $ins->nama_alat = $data['nama_alat'];
                $ins->deleted = 0;
                $ins->save();
            }else{
                DB::table($this->getTableName())->where('id', $data['id'])->update([
                    'nama_alat' => $data['nama_alat']
                ]);
            }
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }

    public function delete(Request $request){
        $data = $request->post();
        
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {        
            //update data
            DB::table($this->getTableName())->where('id', $data['id'])->update([
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }
}
