<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Upt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller{
    public $akses_menu = [];
    public function __construct()
    {
      date_default_timezone_set('Asia/Jakarta');
    $this->akses_menu = json_decode(Cache::store('file')->get('akses_menu'));
    }

    public function getHeaderCss()
    {
        return array(
            'css-1' => asset('assets/css/leaflet/leaflet.css'),
            'css-2' => asset('assets/css/fselect/fselect.css'),
            'js-1' => asset('assets/js/leaflet/leaflet.js'),
            'js-2' => asset('assets/js/fselect/fselect.js'),
            'js-3' => asset('assets/js/controllers/dashboard.js'),
        );
    }
    
    public function getListUpt(){
        $datadb = Upt::where('nama_upt', '!=', 'SUPERADMIN')->get();
        return $datadb;
    }

    public function index(Request $req){
        $data['header_data'] = $this->getHeaderCss();     
        $data['title_content'] = 'Dashboard';
        $data['title_top'] = 'Dashboard';   
        $data['akses'] = $this->akses_menu;
        $data['list_upt'] = $this->getListUpt();
        // echo '<pre>';
        // print_r($data);die;
        $data['view_file'] = view('dashboard.index', $data);
        return view('template.main', $data);
    }
}
