<?php

namespace App\Http\Controllers\Anomali;

use App\Bay;
use App\Critical_bay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CriticalBayController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/controllers/anomali/critical_bay.js'),
        );
    }

    public function getTitleParent(){
        return "Anomali";
    }

    public function getTableName(){
        return "critical_bay";
    }

    public function index(){
        $data['data'] = [];
        $view = view('critical_bay.index', $data);

        $put['title_content'] = 'Critical Bay';
        $put['title_top'] = 'Critical Bay';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        return view('template.main', $put);
    }

    public function getListBay(){
        $data = Bay::where('deleted', '=', '0')->get()->toArray();
        return $data;
    }

    public function add(){
        $data['list_bay'] = $this->getListBay();
        $view = view('critical_bay.formadd', $data);

        $put['title_content'] = 'Form Critical Bay';
        $put['title_top'] = 'Form Critical Bay';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        return view('template.main', $put);
    }

    public function getDetailData($id){
        $data = Critical_bay::where('id', $id)->first();
        return $data;
    }

    public function ubah($id){
        $data = $this->getDetailData($id);
        $data['list_bay'] = $this->getListBay();
        $view = view('critical_bay.formadd', $data);

        $put['title_content'] = 'Form Critical Bay';
        $put['title_top'] = 'Form Critical Bay';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        return view('template.main', $put);
    }
    
    public function getData(){
        DB::enableQueryLog();
        $data['data'] = [];
        $data['recordsTotal'] = 0;
        $data['recordsFiltered'] = 0;
        $garduInduk = DB::table($this->getTableName().' as cb')
        ->select([
            'cb.*',
            'b.nama_bay'
        ])
        ->join('bay as b', 'b.id', '=', 'cb.bay')
        ->where('cb.deleted', '=', '0');
        if(isset($_GET)){            
            $data['recordsTotal'] = $garduInduk->get()->count();
            if(isset($_GET['search']['value'])){
                $keyword = $_GET['search']['value'];
                $garduInduk->where(function($query) use ($keyword){
                    $query->where('b.nama_bay', 'LIKE', '%'.$keyword.'%');
                });
            }
            if(isset($_GET['order'][0]['column'])){
                $garduInduk->orderBy('cb.id', $_GET['order'][0]['dir']);
            }
            $data['recordsFiltered'] = $garduInduk->get()->count();

            if(isset($_GET['length'])){
                $garduInduk->limit($_GET['length']);
            }
            if(isset($_GET['start'])){
                $garduInduk->offset($_GET['start']);
            }
        }
        $data['data'] = $garduInduk->get()->toArray();
        $data['draw'] = $_GET['draw'];
        $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        return json_encode($data);
    }

    public function submit(Request $request){
        // ini_set('memory_limit', -1);
        $data = $request->post();
        // echo '<pre>';
        // print_r($data);die;
        $user_id = Cache::store('file')->get('user_id');
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {
        
            if($data['id'] == ''){
                //insert data
                $ins = new Critical_bay();
                $ins->bay = $data['bay'];
                $ins->apb_risk = $data['apb_risk'];
                $ins->distribution_risk = $data['distribution_risk'];
                $ins->highest_risk = $data['highest_risk'];
                $ins->deleted = 0;
                $ins->save();
            }else{
                DB::table($this->getTableName())->where('id', $data['id'])->update([
                    'bay' => $data['bay'],
                    'apb_risk' => $data['apb_risk'],
                    'distribution_risk' => $data['distribution_risk'],
                    'highest_risk' => $data['highest_risk'],
                ]);
            }
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }

    public function delete(Request $request){
        $data = $request->post();
        
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {        
            //update data
            DB::table($this->getTableName())->where('id', $data['id'])->update([
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }
}
