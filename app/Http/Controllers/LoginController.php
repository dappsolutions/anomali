<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller{
    public function __construct()
    {
        
    }

    public function index(){
        $this->clearCacheLogin();
        return view('login.login');
    }

    public function clearCacheLogin(){
        Cache::flush();
        Artisan::call('cache:clear');
        Cache::store('file')->put('user_id', '');
        Cache::store('file')->put('username', '');
        Cache::store('file')->put('akses', '');
        Cache::store('file')->put('akses_menu', '');
        Cache::store('file')->put('token', '');
    }
}