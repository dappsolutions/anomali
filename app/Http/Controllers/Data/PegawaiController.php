<?php

namespace App\Http\Controllers\Data;

use App\Departemen;
use App\Hak_akses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jabatan;
use App\Pegawai;
use App\Upt;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    public $akses_menu = [];
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->akses_menu = json_decode(Cache::store('file')->get('akses_menu'));
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/controllers/data/pegawai.js'),
        );
    }

    public function getTitleParent(){
        return "Data";
    }

    public function getTableName(){
        return "pegawai";
    }

    public function index(){
        $data['data'] = [];
        $data['akses'] = $this->akses_menu;
        $view = view('pegawai.index', $data);

        $put['title_content'] = 'Pegawai';
        $put['title_top'] = 'Pegawai';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function getListUpt(){
        $data = Upt::where('nama_upt', '!=', 'SUPERADMIN')->get()->toArray();
        return $data;
    }
    
    public function getListDepartemen(){
        $data = Departemen::where('deleted', '=', '0')->get()->toArray();
        return $data;
    }    
    
    public function getListJabatan(){
        $data = Jabatan::where('deleted', '=', '0')->get()->toArray();
        return $data;
    }    

    public function add(){
        $data['list_upt'] = $this->getListUpt();
        $data['list_dept'] = $this->getListDepartemen();
        $data['list_jabatan'] = $this->getListJabatan();
        $data['akses'] = $this->akses_menu;
        $view = view('pegawai.formadd', $data);

        $put['title_content'] = 'Form Pegawai';
        $put['title_top'] = 'Form Pegawai';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }

    public function getDetailData($id){
        $data = Pegawai::where('id', $id)->first();
        return $data;
    }

    public function ubah($id){
        $data = $this->getDetailData($id);
        $data['list_upt'] = $this->getListUpt();
        $data['list_dept'] = $this->getListDepartemen();
        $data['list_jabatan'] = $this->getListJabatan();
        $data['akses'] = $this->akses_menu;
        $view = view('pegawai.formadd', $data);

        $put['title_content'] = 'Form Pegawai';
        $put['title_top'] = 'Form Pegawai';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        $put['akses'] = $this->akses_menu;
        return view('template.main', $put);
    }
    
    public function getData(){
        DB::enableQueryLog();
        $data['data'] = [];
        $data['recordsTotal'] = 0;
        $data['recordsFiltered'] = 0;
        $garduInduk = DB::table($this->getTableName().' as pg')
        ->select([
            'pg.*',
            'ut.nama_upt'
        ])
        ->join('upt as ut', 'ut.id', '=', 'pg.upt')
        ->where('pg.deleted', '=', '0')
        ->where('ut.nama_upt', '!=', 'SUPERADMIN');
        if(isset($_GET)){            
            $data['recordsTotal'] = $garduInduk->get()->count();
            if(isset($_GET['search']['value'])){
                $keyword = $_GET['search']['value'];
                $garduInduk->where(function($query) use ($keyword){
                    $query->where('pg.nama_pegawai', 'LIKE', '%'.$keyword.'%');
                    $query->where('pg.nip', 'LIKE', '%'.$keyword.'%');
                    $query->orWhere('ut.nama_upt', 'LIKE', '%'.$keyword.'%');
                });
            }
            if(isset($_GET['order'][0]['column'])){
                $garduInduk->orderBy('pg.id', $_GET['order'][0]['dir']);
            }
            $data['recordsFiltered'] = $garduInduk->get()->count();

            if(isset($_GET['length'])){
                $garduInduk->limit($_GET['length']);
            }
            if(isset($_GET['start'])){
                $garduInduk->offset($_GET['start']);
            }
        }
        $data['data'] = $garduInduk->get()->toArray();
        $data['draw'] = $_GET['draw'];
        $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        return json_encode($data);
    }

    public function submit(Request $request){
        // ini_set('memory_limit', -1);
        $data = $request->post();
        // echo '<pre>';
        // print_r($data);die;
        $user_id = Cache::store('file')->get('user_id');
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {
        
            if($data['id'] == ''){
                //insert data
                $ins = new Pegawai();
                $ins->upt = $data['upt'];
                $ins->departemen = $data['departemen'];
                $ins->jabatan = $data['jabatan'];
                $ins->nama_pegawai = $data['nama_pegawai'];
                $ins->nip = $data['nip'];
                $ins->deleted = 0;
                $ins->save();
            }else{
                DB::table($this->getTableName())->where('id', $data['id'])->update([
                    'upt' => $data['upt'],
                    'departemen' => $data['departemen'],
                    'jabatan' => $data['jabatan'],
                    'nama_pegawai' => $data['nama_pegawai'],
                    'nip' => $data['nip']
                ]);
            }
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }

    public function delete(Request $request){
        $data = $request->post();
        
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {        
            //update data
            DB::table($this->getTableName())->where('id', $data['id'])->update([
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }
}
