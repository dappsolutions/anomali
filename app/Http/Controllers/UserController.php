<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    //
    public function login(Request $request)
    {
        Cache::flush();
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $userdata = DB::table('users as usr')
        ->select(['usr.*', 'ha.hak_akses as akses'])
        ->join('hak_akses as ha', 'ha.id', '=', 'usr.hak_akses')
        ->where('usr.email', '=', $request->post('email'))
        ->first();

        //ambil list akses user menu
        $list_akses = DB::table('hak_akses_menu as hak')
        ->join('menu as m', 'm.id', '=', 'hak.menu')
        ->join('hak_akses as ha', 'ha.id', '=', 'hak.hak_akses')
        ->where('ha.id', '=', $userdata->hak_akses)->get()->toArray();

        $result_akses = [];
        foreach ($list_akses as $key => $value) {
            $result_akses[$value->id_menu] = $value;
        }

        Cache::store('file')->put('user_id', $userdata->id);
        Cache::store('file')->put('username', $request->post('email'));
        Cache::store('file')->put('akses', $userdata->akses);
        Cache::store('file')->put('akses_menu', json_encode($result_akses));
        Cache::store('file')->put('token', $token);

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->hak_akses = $request->get('hak_akses');
        $user->pegawai = $request->get('pegawai');
        $user->password = Hash::make($request->get('password'));
        $user->save();
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }
}
