<?php

namespace App\Http\Controllers\Alert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoesNotHasTokenController extends Controller
{
    public $akses_menu = [];
    public function __construct()
    {
      date_default_timezone_set('Asia/Jakarta');
    }

    public function getHeaderCss()
    {
        return array();
    }

    public function index(Request $req){
        $data['header_data'] = $this->getHeaderCss();     
        $data['title_content'] = 'Tidak Punya Token untuk Login';
        $data['title_top'] = 'Tidak Punya Token untuk Login';   
        $data['view_file'] = view('informasi.doesthastoken');
        $data['akses'] = $this->akses_menu;
        return view('template.main', $data);
    }
}
