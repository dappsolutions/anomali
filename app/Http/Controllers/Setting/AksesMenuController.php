<?php

namespace App\Http\Controllers\Setting;

use App\Hak_akses;
use App\Hak_akses_menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AksesMenuController extends Controller
{
    public $akses_menu = [];
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->akses_menu = json_decode(Cache::store('file')->get('akses_menu'));
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/controllers/setting/akses_menu.js'),
        );
    }

    public function getTitleParent(){
        return "Master";
    }

    public function getTableName(){
        return "hak_akses";
    }

    public function index(){
        $data['data'] = [];
        $view = view('akses_menu.index', $data);

        $put['title_content'] = 'Akses Menu';
        $put['title_top'] = 'Akses Menu';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        return view('template.main', $put);
    }

    public function getDetailData($id){
        $data = Hak_akses::where('id', $id)->first();
        return $data;
    }

    public function getListDataMasterMenu($akses_id){
        DB::enableQueryLog(); 
        $datadb = DB::table('menu as m')
        ->select(['m.*', 'hak.id as hak_id', 'hak.create as create', 'hak.update as update', 'hak.delete as delete', 'ha.id as hak_akses'])
        ->leftJoin('hak_akses_menu as hak', function($query){
            return $query->on('hak.menu', '=', 'm.id')
            ->where('hak.deleted', '=', 0);
        })
        ->join('hak_akses as ha', function($query) use ($akses_id){
            return $query->on('hak.hak_akses', '=', 'ha.id')
            ->where('ha.id', '=', $akses_id);
        })
        ->whereNotNull('m.parent')
        ->get()->toArray();

        if(empty($datadb)){
            $datadb = DB::table('menu as m')
            ->select(['m.*', 'hak.id as hak_id', 'hak.create as create', 'hak.update as update', 'hak.delete as delete', 'ha.id as hak_akses'])
            ->leftJoin('hak_akses_menu as hak', function($query){
                return $query->on('hak.menu', '=', 'm.id')
                ->where('hak.deleted', '=', 0);
            })
            ->leftJoin('hak_akses as ha', function($query) use ($akses_id){
                return $query->on('hak.hak_akses', '=', 'ha.id')
                ->where('ha.id', '=', $akses_id);
            })
            ->whereNotNull('m.parent')
            ->get()->toArray();

            $result = [];            
            $temp = [];
            foreach ($datadb as $key => $value) {
                if($value->hak_akses == ''){
                    if(!in_array($value->id, $temp)){
                        array_push($result, $value);
                        $temp[] = $value->id;
                    }
                }
            }
            $datadb = $result;
        }
       

        return $datadb;
    }

    public function ubah($id){
        $data = $this->getDetailData($id);
        $data['akses_id'] = $id;
        $data['list_akses'] = $this->getListDataMasterMenu($id);
        // echo '<pre>';
        // print_r($data);die;
        $view = view('akses_menu.formadd', $data);

        $put['title_content'] = 'Form Akses Menu';
        $put['title_top'] = 'Form Akses Menu';
        $put['title_parent'] = $this->getTitleParent();
        $put['view_file'] = $view;
        $put['header_data'] = $this->getHeaderCss();
        return view('template.main', $put);
    }
    
    public function getData(){
        DB::enableQueryLog();
        $data['data'] = [];
        $data['recordsTotal'] = 0;
        $data['recordsFiltered'] = 0;
        $garduInduk = DB::table($this->getTableName().' as b')
        ->select([
            'b.*'
        ]);
        if(isset($_GET)){            
            $data['recordsTotal'] = $garduInduk->get()->count();
            if(isset($_GET['search']['value'])){
                $keyword = $_GET['search']['value'];
                $garduInduk->where(function($query) use ($keyword){
                    $query->where('b.hak_akses', 'LIKE', '%'.$keyword.'%');
                });
            }
            if(isset($_GET['order'][0]['column'])){
                $garduInduk->orderBy('b.id', $_GET['order'][0]['dir']);
            }
            $data['recordsFiltered'] = $garduInduk->get()->count();

            if(isset($_GET['length'])){
                $garduInduk->limit($_GET['length']);
            }
            if(isset($_GET['start'])){
                $garduInduk->offset($_GET['start']);
            }
        }
        $data['data'] = $garduInduk->get()->toArray();
        $data['draw'] = $_GET['draw'];
        $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        return json_encode($data);
    }

    public function submit(Request $request){
        // ini_set('memory_limit', -1);
        $data = $request->post();
        // echo '<pre>';
        // print_r($data);die;
        $user_id = Cache::store('file')->get('user_id');
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {
            foreach ($data['data_menu'] as $key => $value) {
                if($value['hak_id'] == ''){
                    $ins = new Hak_akses_menu();
                    $ins->hak_akses = $data['id'];
                    $ins->menu = $value['menu_id'];
                    $ins->create = $value['create'];
                    $ins->update = $value['update'];
                    $ins->delete = $value['delete'];
                    $ins->deleted = 0;
                    $ins->save();
                }else{
                    DB::table('hak_akses_menu')->where('id', $value['hak_id'])->update([
                        'create' => $value['create'],
                        'update' => $value['update'],
                        'delete' => $value['delete'],
                    ]);
                }
            }
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }

    public function delete(Request $request){
        $data = $request->post();
        
        $result['is_valid'] = false;
        DB::beginTransaction();
        try {        
            //update data
            DB::table($this->getTableName())->where('id', $data['id'])->update([
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            DB::commit();
            $result['is_valid'] = true;
        } catch (\Throwable $th) {
            $result['message'] = $th->getMessage();
            DB::rollBack();
        }

        echo json_encode($result);
    }
}
