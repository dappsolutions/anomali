<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login.login');
// });

//tidak punyak token
Route::get('/token_not_found', 'Alert\DoesNotHasTokenController@index');

//login
Route::get('/', 'LoginController@index');
Route::get('login', 'LoginController@index');
Route::post('login/signIn', 'LoginController@signIn');

//dashboard
Route::get('dashboard', 'DashboardController@index')->middleware('cek.login');
Route::get('dashboard/index', 'DashboardController@index')->middleware('cek.login');

//master
//gardu induk
Route::get('master/gardu_induk', 'Master\GarduIndukController@index')->middleware('cek.login');
Route::get('master/gardu_induk/index', 'Master\GarduIndukController@index')->middleware('cek.login');
Route::get('master/gardu_induk/add', 'Master\GarduIndukController@add')->middleware('cek.login');
Route::get('master/gardu_induk/ubah/{id}', 'Master\GarduIndukController@ubah')->middleware('cek.login');
//bay
Route::get('master/bay', 'Master\BayController@index')->middleware('cek.login');
Route::get('master/bay/index', 'Master\BayController@index')->middleware('cek.login');
Route::get('master/bay/add', 'Master\BayController@add')->middleware('cek.login');
Route::get('master/bay/ubah/{id}', 'Master\BayController@ubah')->middleware('cek.login');
//phasa
Route::get('master/phasa', 'Master\PhasaController@index')->middleware('cek.login');
Route::get('master/phasa/index', 'Master\PhasaController@index')->middleware('cek.login');
Route::get('master/phasa/add', 'Master\PhasaController@add')->middleware('cek.login');
Route::get('master/phasa/ubah/{id}', 'Master\PhasaController@ubah')->middleware('cek.login');
//alat
Route::get('master/alat', 'Master\AlatController@index')->middleware('cek.login');
Route::get('master/alat/index', 'Master\AlatController@index')->middleware('cek.login');
Route::get('master/alat/add', 'Master\AlatController@add')->middleware('cek.login');
Route::get('master/alat/ubah/{id}', 'Master\AlatController@ubah')->middleware('cek.login');
//departemen
Route::get('data/departemen', 'Data\DepartemenController@index')->middleware('cek.login');
Route::get('data/departemen/index', 'Data\DepartemenController@index')->middleware('cek.login');
Route::get('data/departemen/add', 'Data\DepartemenController@add')->middleware('cek.login');
Route::get('data/departemen/ubah/{id}', 'Data\DepartemenController@ubah')->middleware('cek.login');
//jabatan
Route::get('data/jabatan', 'Data\JabatanController@index')->middleware('cek.login');
Route::get('data/jabatan/index', 'Data\JabatanController@index')->middleware('cek.login');
Route::get('data/jabatan/add', 'Data\JabatanController@add')->middleware('cek.login');
Route::get('data/jabatan/ubah/{id}', 'Data\JabatanController@ubah')->middleware('cek.login');
//pegawai
Route::get('data/pegawai', 'Data\PegawaiController@index')->middleware('cek.login');
Route::get('data/pegawai/index', 'Data\PegawaiController@index')->middleware('cek.login');
Route::get('data/pegawai/add', 'Data\PegawaiController@add')->middleware('cek.login');
Route::get('data/pegawai/ubah/{id}', 'Data\PegawaiController@ubah')->middleware('cek.login');
//critical_bay
Route::get('anomali/critical_bay', 'Anomali\CriticalBayController@index')->middleware('cek.login');
Route::get('anomali/critical_bay/index', 'Anomali\CriticalBayController@index')->middleware('cek.login');
Route::get('anomali/critical_bay/add', 'Anomali\CriticalBayController@add')->middleware('cek.login');
Route::get('anomali/critical_bay/ubah/{id}', 'Anomali\CriticalBayController@ubah')->middleware('cek.login');
//anomali
Route::get('anomali/anomali', 'Anomali\AnomaliController@index')->middleware('cek.login');
Route::get('anomali/anomali/index', 'Anomali\AnomaliController@index')->middleware('cek.login');
Route::get('anomali/anomali/add', 'Anomali\AnomaliController@add')->middleware('cek.login');
Route::get('anomali/anomali/ubah/{id}', 'Anomali\AnomaliController@ubah')->middleware('cek.login');
//akses_menu
Route::get('setting/akses_menu', 'Setting\AksesMenuController@index')->middleware('cek.login');
Route::get('setting/akses_menu/index', 'Setting\AksesMenuController@index')->middleware('cek.login');
Route::get('setting/akses_menu/ubah/{id}', 'Setting\AksesMenuController@ubah')->middleware('cek.login');

