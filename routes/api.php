<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('book', 'BookController@book');

Route::get('bookall', 'BookController@bookAuth')->middleware('jwt.verify');
Route::get('user', 'UserController@getAuthenticatedUser')->middleware('jwt.verify');



//master
//gardu induk
Route::get('master/gardu_induk/getData', 'Master\GarduIndukController@getData');
Route::post('master/gardu_induk/submit', 'Master\GarduIndukController@submit');
Route::post('master/gardu_induk/delete', 'Master\GarduIndukController@delete');
Route::post('master/gardu_induk/getListUpt', 'Master\GarduIndukController@getListUptApi');

//bay
Route::get('master/bay/getData', 'Master\BayController@getData');
Route::post('master/bay/submit', 'Master\BayController@submit');
Route::post('master/bay/delete', 'Master\BayController@delete');
//phasa
Route::get('master/phasa/getData', 'Master\PhasaController@getData');
Route::post('master/phasa/submit', 'Master\PhasaController@submit');
Route::post('master/phasa/delete', 'Master\PhasaController@delete');
//alat
Route::get('master/alat/getData', 'Master\AlatController@getData');
Route::post('master/alat/submit', 'Master\AlatController@submit');
Route::post('master/alat/delete', 'Master\AlatController@delete');

//data
//departemen
Route::get('data/departemen/getData', 'Data\DepartemenController@getData');
Route::post('data/departemen/submit', 'Data\DepartemenController@submit');
Route::post('data/departemen/delete', 'Data\DepartemenController@delete');
//jabatan
Route::get('data/jabatan/getData', 'Data\JabatanController@getData');
Route::post('data/jabatan/submit', 'Data\JabatanController@submit');
Route::post('data/jabatan/delete', 'Data\JabatanController@delete');
//pegawai
Route::get('data/pegawai/getData', 'Data\PegawaiController@getData');
Route::post('data/pegawai/submit', 'Data\PegawaiController@submit');
Route::post('data/pegawai/delete', 'Data\PegawaiController@delete');
//critical_bay
Route::get('anomali/critical_bay/getData', 'Anomali\CriticalBayController@getData');
Route::post('anomali/critical_bay/submit', 'Anomali\CriticalBayController@submit');
Route::post('anomali/critical_bay/delete', 'Anomali\CriticalBayController@delete');
Route::post('data/pegawai/delete', 'Data\PegawaiController@delete');
//anomali
Route::get('anomali/anomali/getData', 'Anomali\AnomaliController@getData');
Route::post('anomali/anomali/submit', 'Anomali\AnomaliController@submit');
Route::post('anomali/anomali/delete', 'Anomali\AnomaliController@delete');
//akses_menu
Route::get('setting/akses_menu/getData', 'Setting\AksesMenuController@getData');
Route::post('setting/akses_menu/submit', 'Setting\AksesMenuController@submit');
Route::post('setting/akses_menu/delete', 'Setting\AksesMenuController@delete');
