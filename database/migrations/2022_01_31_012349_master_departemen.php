<?php

use App\Departemen;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterDepartemen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('departemen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_departemen', 255);
            $table->integer('deleted');
            $table->timestamps();
        });

        $data = ['SUPERADMIN'];
        foreach ($data as $key => $value) {
            $jabatan = new Departemen();
            $jabatan->nama_departemen = $value;
            $jabatan->deleted = 0;
            $jabatan->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('departemen');
    }
}
