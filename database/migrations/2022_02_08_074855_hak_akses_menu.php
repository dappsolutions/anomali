<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HakAksesMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //]
        Schema::create('hak_akses_menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('hak_akses');
            $table->integer('deleted');
            $table->integer('menu');
            $table->integer('create');
            $table->integer('update');
            $table->integer('delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('hak_akses_menu');
    }
}
