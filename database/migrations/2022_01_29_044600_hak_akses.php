<?php

use App\Hak_akses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HakAkses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hak_akses', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('hak_akses', 255);
            $table->integer('deleted');
        });

        $data = ['superadmin', 'guest', 'spv', 'multg', 'mrenev', 'mupt', 'hartrans', 'srm_hartrans', 'other'];

        foreach ($data as $key => $value) {        
            $hak_akses = new Hak_akses();
            $hak_akses->hak_akses = $value;
            $hak_akses->deleted = 0;
            $hak_akses->save();
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('hak_akses');
    }
}
