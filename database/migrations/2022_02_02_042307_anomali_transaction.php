<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnomaliTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('anomali_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gardu_induk');
            $table->integer('document');
            $table->integer('bay');
            $table->integer('jenis_peralatan');
            $table->integer('trafic');
            $table->integer('phasa');
            $table->string('uraian', 255);
            $table->integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('anomali_transaction');
    }
}
