<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterCriticalBay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('critical_bay', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bay');
            $table->integer('apb_risk');
            $table->integer('distribution_risk');
            $table->integer('highest_risk');
            $table->integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('critical_bay');
    }
}
