<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnomaliTransactionItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('anomali_transaction_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('anomali_transaction');
            $table->integer('alat');
            $table->integer('alat_subsystem');
            $table->integer('alat_subsystem_anomali');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('anomali_transaction_item');
    }
}
