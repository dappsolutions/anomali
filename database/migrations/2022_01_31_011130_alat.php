<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Alat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('alat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gardu_induk')->nullable();
            $table->integer('merk_alat');
            $table->integer('deleted');
            $table->string('nama_alat', 255)->nullable();
            $table->string('kode_alat', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('alat');
    }
}
