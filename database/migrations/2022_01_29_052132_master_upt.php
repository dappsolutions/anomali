<?php

use App\Upt;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterUpt extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('upt',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama_upt', 255);
            $table->integer('deleted');            
        });

        $data_upt = [
            'KANTOR INDUK', 
            'UNIT PROBOLINGGO', 
            'UNIT MALANG', 
            'UNIT SURABAYA', 
            'UNIT GRESIK', 
            'UNIT BALI',
            'UNIT MADIUN',
            'SUPERADMIN'
        ];

        foreach($data_upt as $value){
            $upt = new Upt();
            $upt->nama_upt = $value;
            $upt->deleted = 0;
            $upt->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('upt');
    }
}
