<?php

use App\Satuan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterSatuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('satuan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_satuan', 255);
            $table->integer('deleted');
            $table->timestamps();
        });

        $data = ['PCS'];
        foreach ($data as $key => $value) {
         $sat = new Satuan();
         $sat->nama_satuan = $value;
         $sat->deleted = 0;
         $sat->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('satuan');
    }
}
