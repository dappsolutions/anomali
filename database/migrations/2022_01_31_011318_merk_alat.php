<?php

use App\Merk_alat;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MerkAlat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('merk_alat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('deleted');
            $table->string('merk', 255);
            $table->timestamps();
        });

        $dataMerk = ['TES'];
        foreach ($dataMerk as $key => $value) {
            $merk = new Merk_alat();
            $merk->merk = $value;
            $merk->deleted = 0;
            $merk->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('merk_alat');
    }
}
