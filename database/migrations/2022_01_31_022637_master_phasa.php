<?php

use App\Phasa;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPhasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('phasa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_phasa');
            $table->integer('deleted');
            $table->timestamps();
        });

        $data = ['R'];
        foreach ($data as $key => $value) {
            $phasa = new Phasa();
            $phasa->nama_phasa = $value;
            $phasa->deleted = 0;
            $phasa->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('phasa');
    }
}
