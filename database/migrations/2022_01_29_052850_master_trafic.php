<?php

use App\Trafic;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterTrafic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('trafic', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('jenis_trafic', 255);
            $table->integer('deleted');           
        });

        $data_trafic = ['Need Repair', 'Need Attention'];
        foreach ($data_trafic as $key => $value) {
            $traf = new Trafic();
            $traf->jenis_trafic = $value;
            $traf->deleted = 0;
            $traf->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('trafic');
    }
}
