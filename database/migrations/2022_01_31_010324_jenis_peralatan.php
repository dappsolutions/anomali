<?php

use App\Jenis_peralatan;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JenisPeralatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('jenis_peralatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jenis', 255);
            $table->integer('deleted');
            $table->timestamps();
        });

        $dataPeralatan = ['CT', 'LA', 'PMT'];
        foreach ($dataPeralatan as $key => $value) {
            $jp = new Jenis_peralatan();
            $jp->jenis = $value;
            $jp->deleted = 0;
            $jp->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('jenis_peralatan');
    }
}
