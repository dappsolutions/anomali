<?php

use App\Jabatan;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterJabatan extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('jabatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_jabatan', 255);
            $table->integer('deleted');
            $table->timestamps();
        });

        $data = ['SUPERADMIN'];
        foreach ($data as $key => $value) {
            $jabatan = new Jabatan();
            $jabatan->nama_jabatan = $value;
            $jabatan->deleted = 0;
            $jabatan->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('jabatan');
    }
}
