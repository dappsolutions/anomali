<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlatSubsystemAnomali extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('alat_subsystem_anomali', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('deleted');
            $table->integer('alat_subsystem');
            $table->string('anomali', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('alat_subsystem_anomali');
    }
}
